package com.udea.util;

public class Util {
    
    /**
     * Get the data type of any object
     *
     * @param var variable
     * @return datatype
     */
    public static String getTypeInfo(Object var) {
        final String type = var.getClass().getTypeName();
        final boolean isPrimitivo = var.getClass().isPrimitive();

        String min_value = "";
        String max_value = "";
        if (var instanceof Short) {
            min_value = Short.MIN_VALUE + "";
            max_value = Short.MAX_VALUE + "";
        } else if (var instanceof Integer) {
            min_value = Integer.MIN_VALUE + "";
            max_value = Integer.MAX_VALUE + "";
        } else if (var instanceof Long) {
            min_value = Long.MIN_VALUE + "";
            max_value = Long.MAX_VALUE + "";
        } else if (var instanceof Float) {
            min_value = Float.MIN_VALUE + "";
            max_value = Float.MAX_VALUE + "";
        } else if (var instanceof Double) {
            min_value = Double.MIN_VALUE + "";
            max_value = Double.MAX_VALUE + "";
        } else if (var instanceof Character) {
            min_value = Character.MIN_VALUE + "";
            max_value = Character.MAX_VALUE + "";
        } else if (var instanceof Byte) {
            min_value = Byte.MIN_VALUE + "";
            max_value = Byte.MAX_VALUE + "";
        } else if (var instanceof Boolean) {
            min_value = Boolean.FALSE + "";
            max_value = Boolean.TRUE + "";
        }

        return new StringBuilder("Tipo de dato: ")
                .append(type).append(", ")
                .append("Valor mínimo: ").append(min_value).append(", ")
                .append("Valor maximo: ").append(max_value).append("\n").toString();
    }

}
