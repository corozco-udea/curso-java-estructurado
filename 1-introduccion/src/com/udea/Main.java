package com.udea;

import com.udea.funciones.Funciones;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("Hola mundo");
        // Introduccion.execute();

        String[] param = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
        //Condicionales.execute(param);

        //String[] param = {};
        //Ciclos.execute(param);

        Integer[] A = new Integer[]{1, 2, 3};
        Integer[] B = new Integer[]{2, 3, 3};
        int dot = Funciones.dot_product_R3(A, B);
        System.out.println(dot);
    }
}
