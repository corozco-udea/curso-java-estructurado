package com.udea.introduccion;

import com.udea.util.Util;

public class Introduccion {

    public static void execute(){

        // Estructuras primitivas
        // short, int, long, float, double, boolean, char, byte

        //Variables numericas
        short a_short = 10;
        int a_int = 10;
        long a_long = 10;
        float a_float = 10;
        double a_double = 10;
        int b = 12;

        //Banderas, variables de control
        boolean flag_true = 5 > 2;
        boolean flag_false = 2 > 10;

        System.out.println("5 es mayor que 2: " + flag_true);
        System.out.println("2 es mayor que 10: " + flag_false);

        System.out.println("a_short: " + a_short + ", tipo: " + Util.getTypeInfo(a_short));
        System.out.println("a_int: " + a_int + ", tipo: " + Util.getTypeInfo(a_int));
        System.out.println("a_long: " + a_long + ", tipo: " + Util.getTypeInfo(a_long));
        System.out.println("a_float: " + a_float + ", tipo: " + Util.getTypeInfo(a_float));
        System.out.println("a_double: " + a_double + ", tipo: " + Util.getTypeInfo(a_double));

        short b_short = 0;
        System.out.println(b_short);

        //Estructuras compuestas
        //String
        String c = "Hola";

        //Estructuras primitivas compuestas (Wrappers)
        Integer b_integer_compuesto = 10;

        Integer a_short_compuesto = 10;
        Short b_short_compuesto = 0;
        Short c_short_compuesto = 3;

        System.out.println(b_short_compuesto);

        //Operador + (operaciones aritméticas, concatenación)
        System.out.println(a_int + b);
        System.out.println(a_int + b + c);

        //Operaciones
        short suma = (short) (b_short_compuesto + b_integer_compuesto.shortValue());
        System.out.println("resultado suma: " + suma);

        Util.dividir(b_short_compuesto, b_integer_compuesto.shortValue());
        Util.dividir(a_short_compuesto.shortValue(), c_short_compuesto);

        short suma_out_range = (short) (a_short_compuesto + Short.MAX_VALUE);
        System.out.println("resultado suma: " + suma_out_range);

        float suma_in_range = a_short_compuesto + Short.MAX_VALUE;
        System.out.println("resultado suma: " + suma_in_range);

        float division_float = a_short_compuesto.floatValue()/c_short_compuesto;
        System.out.println("resultado division: " + division_float);

        // El IDE permite identificar problemas a priori
        int divisor = 0;
        //int res = b / divisor;
    }
}
