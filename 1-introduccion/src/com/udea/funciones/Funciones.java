package com.udea.funciones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Funciones {

    public static void execute(final String[] args) throws IOException {

    }

    /**
     * Calculate the dot product between two R3 integer vectors
     * @param A vector
     * @param B vector
     * @return C with the dot product of A,B
     * @exception return an error message
     */
    public static int dot_product_R3(final Integer[] A, final Integer[] B) {
        if(A.length == B.length && A.length == 3) {
            int result = A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
            print_resultado("A dot B = "+ result);
            return result;
        } else {
            print_resultado("Este producto punto está definido solo para dos vectores en R3");
            return -1;
        }
    }

    public static void print_resultado(String msg) {
        System.out.println(msg);
    }

}
