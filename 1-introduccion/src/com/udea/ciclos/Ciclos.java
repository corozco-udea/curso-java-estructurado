package com.udea.ciclos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ciclos {

    private static final BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));

    public static void execute(final String[] args) throws IOException {
        System.out.print("1 ");
        System.out.print("2 ");
        System.out.print("3 ");
        System.out.print("4 ");
        System.out.print("5 ");
        System.out.print("6 ");

        // for_cycle();
        // while_cycle();
        // do_while_cycle();

    }

    //Sumatorias, recorrer elementos, patrones iterativos
    private static void for_cycle() {

        System.out.println("");
        for (int i = 0; i < 6; i++)
            System.out.print((i + 1) + " ");

        System.out.println("");
        final int n = 100;
        for (int i = 0; i < n + 1; i++) {
            if (i % 2 == 0)
                System.out.print(i + " ");
        }

        System.out.println("");
        for (int i = n; i > -1; i--) {
            if (i % 2 == 0)
                System.out.print(i + " ");
        }

        System.out.println("");
        for (int i = 0; i < n + 1; i = i + 2)
            System.out.print(i + " ");

        System.out.println("");
        int suma = (n * (n + 1)) / 2;
        System.out.println(suma);

        suma = 0;
        for (int i = 0; i < n + 1; i++)
            suma += i;
        System.out.println(suma);

        String[] planetas = {"Mercurio", "Venus", "Tierra", "Marte", "Jupíter", "Saturno", "Urano", "Neptuno"};
        System.out.println("");
        for (final String planeta : planetas)
            System.out.print(planeta + " ");
    }

    private static void while_cycle() throws IOException {
        int i = 0;
        System.out.println("");
        while (i < 6) {
            System.out.print((i + 1) + " ");
            i += 1;
        }

        System.out.println("");
        final int n = 100;
        i = 0;
        while (i < n + 1) {
            if (i % 2 == 0)
                System.out.print(i + " ");
            i++;
        }

        System.out.println("");
        i = 0;
        while (i < n + 1) {
            System.out.print(i + " ");
            i += 2;
        }

        System.out.println("");
        i = n;
        while (i > -1) {
            System.out.print(i + " ");
            i -= 2;
        }

        System.out.println("");
        boolean flag = true;
        while (flag) {
            System.out.print("Ingrese un valor: ");
            int var_control = Integer.parseInt(teclado.readLine());
            if (var_control == 0) {
                flag = false;
                System.out.println("Exit");
            } else {
                System.out.println("El valor no es 0'");
            }
        }

        float saldo = 20000;
        System.out.println("Saldo de jimena: " + saldo);
        while (saldo > 2750) {
            saldo -= 2750;
            System.out.println("Pasó la tarjeta, nuevo saldo: " + saldo);
        }
        System.out.println("La tarjeta no tiene saldo suficiente");
    }

    private static void do_while_cycle() throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String opcion = "";
        do {
            System.out.println("Menu");
            System.out.println("1. Suma");
            System.out.println("2. Resta");
            System.out.println("3. Multiplicacion");
            System.out.println("4. Division");
            System.out.println("0. Salir");
            System.out.print("Ingrese la opción: ");
            opcion = br.readLine();
        } while (!opcion.equals("0"));
    }
}
