package com.udea.condicionales;

public class Condicionales {

    public static void execute(String[] args) {
        int a = 10;
        int b = 9;

        boolean flag = a > b;

        //condicionales basicos
        if (flag) {
            System.out.println("El mayor es " + a);
        } else if (a < b) {
            System.out.println("El mayor es " + b);
        } else {
            System.out.println("Son iguales");
        }

        //operaciones con booleanos
        boolean isTrue = false || false || false || false || false || true;
        boolean isFalse = true && true && true && true && true && false;

        //Operador ternario
        String flag_msg = a > b ? "El mayor es " + a : a < b ? "El mayor es " + b : "Son iguales";

        System.out.println(flag_msg);

        System.out.println("isTrue: " + isTrue);
        System.out.println("isFalse: " + isFalse);

        //Switch
        for (int i = 0; i < args.length; i++) {
            String month = getMonthFromIndex(Integer.parseInt(args[i]));
            System.out.print(month + " ");
        }

    }

    private static String getMonthFromIndex(int value) {
        switch (value) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                return "Es valor no valido";
        }
    }
}
