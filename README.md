## Introducción

Este proyecto tiene el propósito de presentar los conceptos básicos-itermedios de la programación estructurada utilizando lenguaje java.

El proyecto tiene la siguiente estructura

* curso-java avanzado
  * sesion-1
    * Java proyect
  * sesion-2
    * Java proyect
  * ...
  * sesion-n
      * Java proyect

## Pre requisitos

* Intellij IDEA latest version
* Git latest version
* Java SDK version 11

## Iniciar proyecto

1. Ir a la carpeta con el proyecto deseado (Ej. 1-introducción)
2. Ir a la clase main
3. Ejecutar

## Notas del autor

La finalidad de este proyecto es académico y el código es de libre acceso. No obsante, se solicita que cualquier colaborador que haga uso del proyecto referencia al autor.

* **Autor:** PhD(c). MSc. Eng. Carlos Eduardo Orozco
* **Contacto:** +573116345180
* **Correo institucional:**
  * carlosorozco@unicauca.edu.co
  * carlose.orozco@udea.edu.co
* **Correo personal:**
  * carlos940807@gmail.com